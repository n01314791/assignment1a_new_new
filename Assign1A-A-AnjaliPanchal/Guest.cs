﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assign1A_A_AnjaliPanchal
{
    public class Guest
    {
        //Basic information of the guest

        private string guestFirstName;
        private string guestLastName;
        private string guestEmail;
        private string guestPhone;
        private string gender;
        private int noOfAdults;
        private string guestRole;
        private double totalPrice;

        public Guest(int no, double p)
        {
            noOfAdults = no;
            totalPrice = p;
        }

        //property accessors

        public string GuestFirstName
        {
            get { return guestFirstName; }
            set { guestFirstName = value; }
        }
        public string GuestLastName
        {
            get { return guestLastName; }
            set { guestLastName = value; }
        }
        public string GuestEmail
        {
            get { return guestEmail; }
            set { guestEmail = value; }
        }
        public string GuestPhone
        {
            get { return guestPhone; }
            set { guestPhone = value; }
        }
        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        public int NoOfAdults
        {
            get { return noOfAdults; }
            set { noOfAdults = value; }
        }
        public string GuestRole
        {
            get { return guestRole; }
            set { guestRole = value; }
        }
        public double TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }
    }
}