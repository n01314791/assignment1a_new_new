﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assign1A_A_AnjaliPanchal
{
    public class Charity
    {
        public Fund fund;
        public Guest guest;

        public Charity(Fund f, Guest g)
        {
            fund = f;
            guest = g;
        }

        public string PrintReceipt()
        {
            string receipt = "<b> Charity Event Receipt : </b><br/>";
            receipt += "You total amount to pay is :" + CalculateAmount().ToString() + "<br/>";
            receipt += "Name : " + guest.GuestFirstName + " " + guest.GuestLastName + "<br/>";
            receipt += "Email : " + guest.GuestEmail + "<br/>";
            receipt += "Mobile No :" + guest.GuestPhone + "<br/>";
            receipt += "Ticket Amounnt :" + TicketAmount().ToString() + "<br/>";
            receipt += "Donation Amount :" + fund.fundAmount + "<br/>";
            return receipt;
        }

        public double TicketAmount()
        {
            double tickettotal = 0;

            tickettotal += guest.NoOfAdults * 10.5;

            return tickettotal;
        }

        public double CalculateAmount()
        {
            double amount = 0;

            amount += TicketAmount();
            amount += fund.fundAmount;

            return amount;
        }


    }
}