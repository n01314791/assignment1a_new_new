﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CharityEvent.aspx.cs" Inherits="Assign1A_A_AnjaliPanchal.CharityEvent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>Charity Event</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Registration Form</h1>
        <h3 id="info" runat="server"></h3>
        <div>
            <b>
                <asp:Label Text="First Name :" ID="lblfname" runat="server"></asp:Label></b>
            <asp:TextBox ID="guestfname" runat="server"></asp:TextBox>
            <%--<asp:ValidationSummary ForeColor="Red" runat="server" ID="validationsummary" />--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" runat="server" ErrorMessage="First Name required"
                ControlToValidate="guestfname"></asp:RequiredFieldValidator>
        </div>
        <div>
            <b>
                <asp:Label Text="Last Name :" ID="lbllname" runat="server"></asp:Label></b>
            <asp:TextBox ID="guestlname" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ErrorMessage="Last Name required"
                ControlToValidate="guestlname"></asp:RequiredFieldValidator>
        </div>
        <div>
            <b>
                <asp:Label Text="E-Mail :" ID="lblemail" runat="server"></asp:Label></b>
            <asp:TextBox ID="guestemail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="Red" ErrorMessage="Email required"
                ControlToValidate="guestemail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="Red"
                ErrorMessage="Email is not in proper format" ControlToValidate="guestemail"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

        </div>
        <%--<div>
            <b>
                <asp:Label Text="Password :" ID="lblpassword" runat="server"></asp:Label></b>
            <asp:TextBox ID="guestpassword" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="Red" ErrorMessage="Password required"
                ControlToValidate="txtpassword"></asp:RequiredFieldValidator>

        </div>--%>
        <%--<div>
            <b>
                <asp:Label Text="Confirm Password :" ID="lblcnfmpassword" runat="server"></asp:Label></b>
            <asp:TextBox ID="guestcnfmpassword" runat="server"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ForeColor="Red"
                ControlToValidate="txtcnfmpassword" ControlToCompare="txtpassword" ErrorMessage="No Match"
                ToolTip="Password must be the same" />

        </div>--%>
        <div>
            <b>
                <asp:Label Text="Mobile No :" ID="lblmobileno" runat="server"></asp:Label></b>
            <asp:TextBox ID="guestmobileno" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="Red" ErrorMessage="Mobile no required"
                ControlToValidate="guestmobileno"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="guestmobileno" Type="String" Operator="NotEqual"
                ValueToCompare="123456789" ErrorMessage="This is an invalid number"></asp:CompareValidator>

        </div>
        <div>
            <b>
                <asp:Label Text="Gender :" ID="lblgender" runat="server"></asp:Label></b>
            <asp:RadioButtonList runat="server" ID="guestgender">
                <asp:ListItem Text="Female" Value="female" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Male" Value="male"></asp:ListItem>
            </asp:RadioButtonList>
            <%--<asp:RadioButton ID="male" Text="Male" GroupName="Gender" runat="server" />
            <asp:RadioButton ID="female" Text="Female" GroupName="Gender" runat="server" />--%>
        </div>
        <div>
            <b>
                <asp:Label Text="Number of Adults Coming" ID="lbladults" runat="server"></asp:Label></b>
            <asp:DropDownList runat="server" ID="noofadults">
                <asp:ListItem Text="1" Value="one"></asp:ListItem>
                <asp:ListItem Text="2" Value="two"></asp:ListItem>
                <asp:ListItem Text="3" Value="three"></asp:ListItem>
                <asp:ListItem Text="4" Value="four"></asp:ListItem>
                <asp:ListItem Text="5" Value="five"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div>
            <b>
                <asp:Label Text="Price per ticket is :$ 10.5 " runat="server" ID="ticketprice"></asp:Label>
                <asp:Label Text="" runat="server" ID="totalprice"></asp:Label>
            </b>
        </div>
        <div>
            <b>
                <asp:Label Text="Role in Event:" ID="lblrole" runat="server"></asp:Label></b>
            <asp:CheckBoxList runat="server" ID="guestrole">
                <asp:ListItem Text="Attendee" Value="Attendee" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Volunteer" Value="Volunteer"></asp:ListItem>
                <asp:ListItem Text="Host" Value="Host"></asp:ListItem>
                <asp:ListItem Text="Speaker" Value="Speaker"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <div>
            <b>
                <asp:Label Text="How Much Fund Would You Like To Give?" ID="lblfund" runat="server"></asp:Label></b>
            <asp:RadioButtonList runat="server" ID="fundamt">
                <asp:ListItem Text="Between 0 to 100" Value="value1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="between 100 to 1000" Value="value2"></asp:ListItem>
                <asp:ListItem Text="Between 1000 to 10000" Value="value3"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div>
            <b>
                <asp:Label Text="Payment Method :" ID="lblpaymethod" runat="server"></asp:Label></b>
            <asp:RadioButtonList runat="server" ID="paymentmethod">
                <asp:ListItem Text="Online" Value="online" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Offline" Value="offline"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:CustomValidator runat="server" ErrorMessage="Online Payment only Sorry..." ControlToValidate="paymentmethod" OnServerValidate="PaymentChoice_Validator"></asp:CustomValidator>
        </div>
        <div>
            <b>
                <asp:Label Text="Amount :" ID="lblamount" runat="server"></asp:Label></b>
            <asp:TextBox ID="fundamount" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ForeColor="Red" ErrorMessage="Amount required"
                ControlToValidate="fundamount"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div>
            <asp:Button Text="Submit" runat="server" ID="btnsubmit" OnClick="Fund" />
        </div>
        <br />
        <div runat="server" id="fundreceipt">

        </div>
        <br/>
        <footer id="footer" runat="server">
        </footer>
    </form>
</body>
</html>
