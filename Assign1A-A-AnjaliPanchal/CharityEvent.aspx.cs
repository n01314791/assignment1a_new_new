﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Assign1A_A_AnjaliPanchal
{
    public partial class CharityEvent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string slogan = "Be A Part Of The Change...";
            DateTime established = DateTime.ParseExact("1996/08/24","yyyy/MM/dd",CultureInfo.InvariantCulture);
            string tagline = "<br/> Since " + established.ToString("yyyy");

            info.InnerHtml = slogan;
            footer.InnerHtml = tagline;
        }

        protected void PaymentChoice_Validator(object source,ServerValidateEventArgs args)
        {
            if (paymentmethod.SelectedValue == "online")
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Fund(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            info.InnerHtml = "Thank You......";

            string firstName = guestfname.Text.ToString();
            string lastName = guestlname.Text.ToString();
            string email = guestemail.Text.ToString();
            string mobileNo = guestmobileno.Text.ToString();
            string gender = guestgender.SelectedItem.Text.ToString();
            string role = guestrole.SelectedItem.Value.ToString();
            string range = fundamt.SelectedItem.Value.ToString();
            string payMethod = paymentmethod.SelectedItem.Value.ToString();
            int noOfAdults = int.Parse(noofadults.SelectedItem.Text);

            double price = 10.5;
            double amount = double.Parse(fundamount.Text);

            Guest newGuest = new Guest(noOfAdults, price);
            //double totalPrice = (price * noOfAdults) + amount;
            newGuest.GuestFirstName = firstName;
            newGuest.GuestLastName = lastName;
            newGuest.GuestEmail = email;
            newGuest.GuestPhone = mobileNo;
            newGuest.Gender = gender;
            newGuest.GuestRole = role;

            Fund newfund = new Fund();
            newfund.rangeOfFund = range;
            newfund.paymentMethod = payMethod;
            newfund.fundAmount = amount;

            Charity chari = new Charity(newfund,newGuest);

            fundreceipt.InnerHtml = chari.PrintReceipt();
        }
    }
}